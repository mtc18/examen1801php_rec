<!DOCTYPE html>
<html>
<head>
    <title>Lista de Nombres</title>
</head>
<body>
    <h1>Añadir Nombre</h1>
    <form method="post" action="?method=add">
        <label>Nombre:</label>
        <input type="text" name="nombre" value="">
        <br>
        <input type="submit" name="submit"><br>
        <hr>
        <h1>Lista de Nombres: </h1>
        <ul>
            <?php foreach ($nombres as $clave=> $nombre): ?>
                <li><?php echo $nombre ?></li>
                <input type="checkbox" name="nombres[]"
                value="<?php echo $nombre ?>"
                <?php echo in_array($nombre, $_SESSION['nombres']) ? 'checked' : '' ?>
                >
            <?php endforeach?>
        </ul>
        <a href="?method=delete&key=<?php echo $clave ?>">Borrar Nombres</a>
    </form>
</body>
</html>
