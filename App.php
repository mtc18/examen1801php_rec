<?php

class App{

    function __construct(){
        session_start();

        if (isset($_SESSION['nombres'])) {
            $nombres=$_SESSION['nombres'];
        }else{
            $nombres=[];
        }
        require('list.php');
    }

    public function index(){
    }

    public function add(){
        if (isset($_REQUEST['nombre']) && ! empty($_REQUEST['nombre'])){
            $_SESSION['nombres'][]=$_REQUEST['nombre'];
        }
        header('Location:?method=index');
    }

    public function delete(){
        $key=(integer) $_REQUEST['key'];
        unset($_SESSION['nombres'][$key]);
        header('Location:?method=index');
    }

}//fin app
